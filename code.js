// Variable to store the current calculation
let calculation = "";
// Variable to store the last operation performed
let lastOperation = "";

// Function to update the display box
function updateDisplay(value) {
  const display = document.getElementById("box");
  if (display) {
    display.textContent = value;
  }
}

// Function to handle number button clicks
function button_number(number) {
  calculation += number;
  updateDisplay(calculation);
}

// Function to handle operator button clicks
function buttonOperator(operator) {
  if (lastOperation === "=") {
    // If the last operation was "=", start a new calculation
    calculation = "";
    lastOperation = operator;
  } else {
    // Perform the previous calculation and update the display
    calculation = eval(calculation);
    lastOperation = operator;
    updateDisplay(calculation);
  }
}

// Function to handle the equal sign button click
function buttonEqual() {
  calculation = eval(calculation);
  lastOperation = "=";
  updateDisplay(calculation);
}

// Function to clear the current entry
function clearEntry() {
  calculation = "";
  updateDisplay("0");
}

// Function to clear the entire calculation
function button_clear() {
  calculation = "";
  lastOperation = "";
  updateDisplay("0");
}

// Function to remove the last character from the calculation
function backspace_remove() {
  calculation = calculation.slice(0, -1);
  updateDisplay(calculation);
}

// Function to calculate the percentage of the current calculation
function calculate_percentage() {
  calculation = eval(calculation) / 100;
  updateDisplay(calculation);
}

// Function to calculate the square root of the current calculation
function square_root() {
  calculation = Math.sqrt(eval(calculation));
  updateDisplay(calculation);
}

// Function to calculate the square of the current calculation
function power_of() {
  calculation = Math.pow(eval(calculation), 2);
  updateDisplay(calculation);
}

// Function to toggle the sign of the current calculation
function plus_minus() {
  calculation = -eval(calculation);
  updateDisplay(calculation);
}

// Call the clearEntry() function on page load to initialize the display
window.addEventListener("DOMContentLoaded", function () {
  clearEntry();
});
